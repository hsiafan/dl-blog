from datetime import datetime, timedelta
import urllib


def sub_str(string, length):
    """
    :type string: unicode
    """
    if string is None:
        return None
    if len(string) * 2 < length:
        return string
    pos = -1
    curlen = 0
    low = 0x4E00
    high = 0x9FA5
    for ch in string:
        if low <= ord(ch) <= high:
            curlen += 2
        else:
            curlen += 1
        pos += 1
        if curlen > length:
            break
    if pos >= len(string):
        return string
    if pos > 2:
        pos -= 2
    return string[0:pos] + '...'


def encode_tag(tag):
    """
    tag is path segment, so we should encode '/' to %2F. however flask seems cannot get parameter correctly unless
    we encode '/' twice (%252F')
    """
    if not tag:
        return tag
    return urllib.quote(tag.encode('utf-8')).replace('/', '%252F')


def parse_archive(archive):
    """
    get start, and end date from archive
    """
    start_date = datetime.strptime(archive, '%Y-%m') - timedelta(hours=8)
    year, month = (int(item) for item in archive.split('-'))
    if month == 12:
        month = 1
        year += 1
    else:
        month += 1
    end_date = datetime.strptime('%d-%d' % (year, month), '%Y-%m') - timedelta(hours=8)
    return start_date, end_date


def get_archive(date):
    """
    get archive from date
    """
    return (date + timedelta(hours=8)).strftime('%Y-%m')