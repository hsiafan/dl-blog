# coding=utf-8

import inspect
import hashlib
from string import Template

from config import mc
from db import groupdb


def to_str(arg):
    if isinstance(arg, unicode):
        return arg.encode('utf-8')
    else:
        return str(arg)


def _get_key(group, name, function_name, args_dict):
    if name:
        t = Template(name)
        key = t.substitute(args_dict)
    else:
        # this is evil, and not well controlled, so always give a name.
        key = function_name
        if args_dict:
            args_list = sorted(args_dict.items(), key=lambda x: x[0])
            key = key + '-' + '-'.join([t[1] for t in args_list])

    if group:
        key = '%s_%d_%s' % (group, groupdb.get_group_count(group), key)
    if isinstance(key, unicode):
        key = key.encode('utf-8')
    m = hashlib.md5()
    m.update(key)
    key = m.hexdigest()
    return key


def _parse_function(function, args, kwargs):
    args_dict = {}
    if args:
        args_name = inspect.getargspec(function)[0]
        args_dict.update(dict(zip(args_name, args)))
    if kwargs:
        args_dict.update(kwargs)
    for key in args_dict.viewkeys():
        value = args_dict[key]
        if value is None:
            args_dict[key] = ''
        elif isinstance(value, str):
            pass
        elif isinstance(value, unicode):
            args_dict[key] = value.encode('utf-8')
        # hack for object
        elif hasattr(value, 'id'):
            if value.id is not None:
                args_dict[key] = str(value.id)
            else:
                args_dict[key] = ''
        else:
            args_dict[key] = str(args_dict[key])
    return args_dict


def cache(name=None, group=None):
    def _cache(function):
        def wrapper(*args, **kwargs):
            args_dict = _parse_function(function, args, kwargs)
            key = _get_key(group, name, function.__name__, args_dict)
            value = mc.get(key)
            if value is not None:
                return value
            else:
                value = function(*args, **kwargs)
                mc.set(key, value, time=3600 * 24 * 5, min_compress_len=1024)
                return value

        return wrapper

    return _cache


def evict_group(group):
    """evict all group's cache"""

    def _evict_group(function):
        def wrapper(*args, **kwargs):
            value = function(*args, **kwargs)
            groupdb.increase(group)
            return value

        return wrapper

    return _evict_group


def evict(name, group=""):
    """evict specify cache"""

    def _evict(function):
        def wrapper(*args, **kwargs):
            args_dict = _parse_function(function, args, kwargs)
            key = _get_key(group, name, function.__name__, args_dict)
            value = function(*args, **kwargs)
            mc.delete(key)
            return value

        return wrapper

    return _evict