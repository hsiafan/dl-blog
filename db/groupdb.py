# encoding=utf-8
from sqlalchemy.orm import mapper

from db.database import tb_group, db_session
from config import mc

class Group(object):
    """simulate cache group use memcached and database"""

    def __init__(self, key=None, count=None):
        self.id = None
        self.key = key
        self.count = count


mapper(Group, tb_group)


def get_group_count(group):
    key = _get_key(group)
    value = mc.get(key)
    if value is not None:
        return value

    g = db_session.query(Group).filter(tb_group.c.key == group).first()
    if g:
        count = g.count
    else:
        count = 0
    mc.add(key, count)
    return count


def increase(group):
    key = _get_key(group)
    g = db_session.query(Group).filter(tb_group.c.key == group).first()
    if g:
        g.count = (tb_group.c.count + 1) % 100000
        db_session.commit()
    else:
        g = Group(key=group, count=1)
        db_session.add(g)
        db_session.commit()
    mc.set(key, g.count)


def _get_key(group):
    return "group_" + group
