from sqlalchemy import create_engine, MetaData
from sqlalchemy.dialects.mysql import MEDIUMTEXT
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy import Table, Column, Integer, String, DateTime, Text, Index
from config import date_base_url

__author__ = 'dongliu'

engine = create_engine(date_base_url)
metadata = MetaData(engine)

tb_comment = Table('comment', metadata,
                   Column('id', Integer, primary_key=True),
                   Column('postId', Integer, nullable=False),
                   Column('date', DateTime, nullable=False),
                   Column('author', String(64), nullable=True),
                   Column('content', Text, nullable=False),
                   Column('parentContent', Text, nullable=True),
                   Column('username', String(32), nullable=True),
                   Column('homepage', String(128), nullable=True),
                   Column('email', String(64), nullable=True),
                   Column('ip', String(128), nullable=False),
)

Index('by_post', tb_comment.c.postId, tb_comment.c.date, unique=False)

tb_group = Table('group', metadata,
                 Column('id', Integer, primary_key=True),
                 Column('key', String(64), nullable=False, unique=True),
                 Column('count', Integer, nullable=False)
)

tb_tag = Table('tag', metadata,
               Column('id', Integer, primary_key=True),
               Column('cid', String(16), nullable=False),
               Column('name', String(64), nullable=False),
               Column('count', Integer, nullable=False)
)
Index('tag_key', tb_tag.c.cid, tb_tag.c.name, unique=True)

tb_tag_r = Table('tag_r', metadata,
                 Column('id', Integer, primary_key=True),
                 Column('name', String(64), nullable=False),
                 Column('postid', Integer, nullable=False)
)
Index('tag_r_key', tb_tag_r.c.name, tb_tag_r.c.postid, unique=True)
Index('tag_r_post', tb_tag_r.c.postid, unique=False)

tb_post = Table('post', metadata,
                Column('id', Integer, primary_key=True),
                Column('title', String(128), nullable=False),
                Column('date', DateTime, nullable=False),
                Column('author', String(64), nullable=False),
                Column('content', MEDIUMTEXT, nullable=False),
                Column('privilege', Integer, nullable=False),
                Column('last_modify_date', DateTime, nullable=False),
                Column('last_modify_by', String(64), nullable=False),
                Column('commentCount', Integer, nullable=False),
)
Index('post_all', tb_post.c.privilege, tb_post.c.date, unique=False)

tb_file = Table('file', metadata,
                Column('id', Integer, primary_key=True),
                Column('fileid', String(128), nullable=False, unique=True),
                Column('fileName', String(128), nullable=False),
                Column('mimeType', String(128), nullable=False),
                Column('date', DateTime, nullable=False)
)

tb_user = Table('user', metadata,
                Column('id', Integer, primary_key=True),
                Column('username', String(32), nullable=False, unique=True),
                Column('password', String(64), nullable=False),
                Column('email', String(64), nullable=False),
                Column('level', Integer, nullable=False)
)

db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))


def init_db():
    metadata.create_all(bind=engine)


if __name__ == '__main__':
    init_db()