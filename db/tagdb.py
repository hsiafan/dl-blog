# encoding=utf-8
from sqlalchemy import and_
from sqlalchemy.orm import mapper

from db.database import tb_tag, db_session
from tools.decorators import *


# default counter
CID_COUNTER = "counter"
# tag
CID_TAG = "tag"
# archive counter
CID_ARCHIVE = "archive"


class Tag(object):
    def __init__(self, cid=None, name=None, count=None):
        self.cid = cid
        self.name = name
        self.count = count


mapper(Tag, tb_tag)


@cache(group="tag", name="${cid}-${name}")
def get_tag(cid, name):
    return db_session.query(Tag).filter(and_(tb_tag.c.cid == cid, tb_tag.c.name == name)).first()


@cache(group="tag", name="${cid}")
def get_tag_list(cid):
    return db_session.query(Tag).filter(tb_tag.c.cid == cid).all()


@evict_group("tag")
def increase(cid, name):
    tag = get_tag(cid, name)
    if tag:
        tag.count = tb_tag.c.count + 1
    else:
        tag = Tag(cid=cid, name=name, count=1)
        db_session.add(tag)


@evict_group("tag")
def decrease(cid, name):
    tag = get_tag(cid, name)
    if tag:
        if tag.count > 1:
            tag.count = tb_tag.c.count - 1
        else:
            db_session.query(Tag).filter(and_(tb_tag.c.cid == cid, tb_tag.c.name == name)).delete()


def m_save(tag):
    db_session.add(tag)
    db_session.commit()


@evict_group("tag")
def set_tag_count(cid, name, count):
    tag = db_session.query(Tag).filter(and_(tb_tag.c.cid == cid, tb_tag.c.name == name)).first()
    if tag is None and count > 0:
        tag = Tag(cid=cid, name=name, count=count)
        db_session.add(tag)
    elif count == 0:
        db_session.query(Tag).filter(and_(tb_tag.c.cid == cid, tb_tag.c.name == name)).delete()
    else:
        tag.count = count
    db_session.commit()