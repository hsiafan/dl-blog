# encoding=utf-8

from datetime import datetime, timedelta
import re

from sqlalchemy import desc, and_
from sqlalchemy.orm import mapper

from db import tagrdb

from db.database import tb_post, db_session
from tools.decorators import *
from tools import text_util


# deleted
PRIVILEGE_DEL = -1
# hide post
PRIVILEGE_HIDE = 0
# normal post
PRIVILEGE_SHOW = 1
# pinned post
PRIVILEGE_SPIN = 2


class Post(object):
    def __init__(self, id=None, title=None, date=None, author=None, content=None, privilege=None,
                 last_modify_date=None, last_modify_by=None, commentCount=0):
        self.id = id
        self.title = title
        self.date = date
        self.author = author
        self.content = content
        self.privilege = privilege
        self.last_modify_date = last_modify_date if last_modify_date else date
        self.last_modify_by = last_modify_by if last_modify_by else author
        self.commentCount = commentCount
        self.tags = None

    def abstract(self, length=400):
        """
        return abstract of content.
        It is implemented by simple way now.
        """
        content = self.content
        content = re.sub(r'<[^<>]+>', '', content)
        content = text_util.sub_str(content, length * 2)
        return content


mapper(Post, tb_post)


@cache(group="post", name="${privilege}-${offset}-${limit}")
def _get_postid_list(privilege, offset, limit):
    f = tb_post.c.privilege == privilege
    res = db_session.query(tb_post.c.id).filter(f).order_by(desc(tb_post.c.date)).offset(offset).limit(limit).all()
    return [r.id for r in res]


@cache(group="post", name="${privilege}-${tag}-${offset}-${limit}")
def _get_tag_post_ids(privilege, tag, offset, limit):
    # query = db_session.query(Post).join(tb_tag_r.c.postid)
    # res = query.filter(and_(tb_post.c.privilege == privilege, tb_tag_r.c.name == tag)) \
    # .order_by(desc(tb_post.c.date)).offset(offset).limit(limit).all()
    res = db_session.execute(
        '''select post.id from post, tag_r where post.id = tag_r.postid
        and post.privilege = :privilege and tag_r.name = :tag
        order by post.date desc limit :offset, :limit ''',
        {'privilege': privilege, 'tag': tag, 'offset': offset, 'limit': limit})
    return [r.id for r in res]


@cache(group="post", name="${privilege}-${archive}-${offset}-${limit}")
def _get_archive_post_ids(privilege, archive, offset, limit):
    start_date, end_date = text_util.parse_archive(archive)

    f = tb_post.c.privilege == privilege
    f = and_(f, and_(tb_post.c.date >= start_date, tb_post.c.date < end_date))
    res = db_session.query(tb_post.c.id).filter(f).order_by(desc(tb_post.c.date)).offset(offset).limit(limit).all()
    return [r.id for r in res]


def get_post_list(privilege, offset, limit, tag=None, archive=None):
    post_list = []
    if tag is not None:
        post_ids = _get_tag_post_ids(privilege, tag, offset, limit)
    elif archive is not None:
        post_ids = _get_archive_post_ids(privilege, archive, offset, limit)
    else:
        post_ids = _get_postid_list(privilege, offset, limit)
    for post_id in post_ids:
        post = get_post(post_id)
        if post is None:
            post = Post(id=post_id, title="Not Found", content="Not Found")
        post_list.append(post)
    return post_list


@cache(group="post", name="count-${privilege}")
def count(privilege):
    return db_session.query(Post).filter(tb_post.c.privilege == privilege).count()


# @cache(group="post", name="count-${privilege}-${tag}")
def count_tag(privilege, tag):
    res = db_session.execute(
        '''select count(post.id) as xcount from post, tag_r where post.id = tag_r.postid
        and post.privilege = :privilege and tag_r.name = :tag''',
        {'privilege': privilege, 'tag': tag})
    res = [r for r in res]
    return res[0][0]


def count_archive(privilege, archive):
    start_date, end_date = text_util.parse_archive(archive)

    f = tb_post.c.privilege == privilege
    f = and_(f, and_(tb_post.c.date >= start_date, tb_post.c.date < end_date))
    return db_session.query(tb_post.c.id).filter(f).count()


@cache(name="post-${postid}")
def get_post(postid):
    post = db_session.query(Post).filter(tb_post.c.id == postid).first()
    if post is None:
        return post
    post.tags = tagrdb.get_tags(postid)
    return post


@evict_group("post")
@evict(name="post-${post}")
def save_post(post):
    new_post = db_session.merge(post)
    db_session.commit()
    db_session.flush()

    post_id = new_post.id
    if post.tags:
        tagrdb.reset_tags(post_id, post.tags)

    return post_id


def m_save_post(post):
    db_session.add(post)
    if post.tags:
        tagrdb.reset_tags(post.id, post.tags)
    db_session.commit()
    db_session.flush()
