from datetime import datetime, timedelta
import json

from sqlalchemy import desc
from sqlalchemy.orm import mapper

from tools.decorators import *
from db.database import db_session, tb_comment


_gravatar_prefix = 'http://en.gravatar.com/avatar/'


def _get_gravatar_url(email, size=64):
    return _gravatar_prefix + hashlib.md5(email.encode('utf-8')).hexdigest().lower() + "?s=" + str(
        size)


class Comment(object):
    """
    user comment
    """

    def __init__(self, postId=None, date=datetime.today(), author=None, content=None, parentContent=None,
                 username=None, homepage=None, email=None, ip=None):
        self.id = None
        self.postId = postId
        self.date = date
        self.author = author
        self.content = content
        self.parentContent = parentContent
        self.username = username
        self.homepage = homepage
        self.email = email
        self.ip = ip

    def to_dict(self, isadmin):
        """
        convert to dict so we can ser comment to json, used for display.
        """
        if self.email:
            email = self.email
        else:
            email = "default@default.com"
        output = {
            "id": self.id,
            "postId": self.postId,
            "date": (self.date + timedelta(hours=8)).strftime('%Y-%m-%d %H:%M'),
            "content": self.content,
            "username": self.username,
            "homepage": self.homepage,
            "admin": isadmin,
            "avatar": _get_gravatar_url(email)
        }
        if isadmin:
            output["email"] = self.email
            output["ip"] = self.ip
        if self.parentContent:
            pc = json.loads(self.parentContent)
            if pc.get(email):
                pc["email"] = "default@default.com"
            pc["avatar"] = _get_gravatar_url(pc["email"])
            output["parentContent"] = pc
            if not isadmin:
                if pc.get("ip"):
                    del pc["ip"]
                if pc.get("email"):
                    del pc["email"]
        return output


    def to_pdict(self):
        """
        use to generate parent json str
        """
        from datetime import timedelta

        output = {
            "id": self.id,
            "postId": self.postId,
            "date": (self.date + timedelta(hours=8)).strftime('%Y-%m-%d %H:%M'),
            "content": self.content,
            "username": self.username,
            "homepage": self.homepage,
            "email": self.email,
            "ip": self.ip,
        }
        return output


mapper(Comment, tb_comment)


@cache(group="comment", name="${offset}-${limit}")
def get_comments(offset, limit):
    comments = db_session.query(Comment).order_by(desc(tb_comment.c.date)).offset(offset).limit(limit).all()
    return comments


@cache(group="comment", name="${postid}-${offset}-${limit}")
def get_post_comments(postid, offset, limit):
    comments = db_session.query(Comment).filter(tb_comment.c.postId == postid).order_by(desc(tb_comment.c.date)) \
        .offset(offset).limit(limit).all()
    return comments


@evict_group("comment")
def delete_post_comment(postid):
    db_session.query(Comment).filter(tb_comment.c.postId == postid).delete()


@evict_group("comment")
def save_comment(comment):
    db_session.add(comment)
    db_session.commit()
    return comment.id


@evict_group("comment")
def delete_comment(comment_id):
    db_session.query(Comment).filter(tb_comment.c.id == comment_id).delete()


def getcomment(comment_id):
    return db_session.query(Comment).filter(tb_comment.c.id == comment_id).first()

