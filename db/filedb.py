from datetime import datetime

from sqlalchemy import desc
from sqlalchemy.orm import mapper

from db.database import tb_file, db_session
from tools.decorators import *


class File(object):
    def __init__(self, fileid=None, fileName=None, mimeType=None, date=datetime.today()):
        self.id = None
        self.fileid = fileid
        self.fileName = fileName
        self.mimeType = mimeType
        self.date = date


mapper(File, tb_file)


def get_files(offset, limit):
    return db_session.query(File).order_by(desc(tb_file.c.date)).offset(offset).limit(limit).all()


@cache(group="file")
def count():
    return db_session.query(File).count()


@cache(name="file-${fileid}")
def getfile(fileid):
    return db_session.query(File).filter(tb_file.c.fileid == fileid).first()


@evict_group("file")
@evict(name="file-${f}")
def del_file(fileid):
    db_session.query(File).filter(tb_file.c.fileid == fileid).delete()


@evict_group("file")
@evict(name="file-${f}")
def save_file(f):
    ff = getfile(f.fileid)
    if ff is None:
        db_session.add(f)
        db_session.commit()