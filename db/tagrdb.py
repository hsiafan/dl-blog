# encoding=utf-8
from sqlalchemy import and_, desc
from sqlalchemy.orm import mapper

from db.database import db_session, tb_tag_r


class TagR(object):
    def __init__(self, name=None, postid=None):
        self.id = None
        self.name = name
        self.postid = postid


mapper(TagR, tb_tag_r)


def _get_tag_r(name, postid):
    return db_session.query(TagR).filter(and_(tb_tag_r.c.name == name, tb_tag_r.c.postid == postid)).first()


def save_tag_r(name, postid):
    tag_r = _get_tag_r(name, postid)
    if not tag_r:
        tag_r = TagR(name=name, postid=postid)
        db_session.add(tag_r)


def get_tags(postid):
    res = db_session.query(tb_tag_r.c.name).filter(tb_tag_r.c.postid == postid).order_by(desc(tb_tag_r.c.id)).all()
    return [r[0] for r in res]


def reset_tags(post_id, tags):
    db_session.query(TagR).filter(tb_tag_r.c.postid == post_id).delete()
    for tag in tags:
        save_tag_r(tag, post_id)