# encoding=utf-8
from sqlalchemy.orm import mapper

from db.database import db_session, tb_user
from tools.decorators import cache, evict

LEVEL_ADMIN = 10
LEVEL_USER = 0


class User(object):
    """simulate cache group use memcached and database"""

    def __init__(self, username=None, password=None, email=None, level=LEVEL_USER):
        self.id = None
        self.username = username
        self.password = password
        self.email = email
        self.level = level

    def is_admin(self):
        return self.level == LEVEL_ADMIN


mapper(User, tb_user)


@cache(group='user', name='${username}')
def get_user(username):
    return db_session.query(User).filter(tb_user.c.username == username).first()


@evict(group='user', name='${username}')
def save_user(username, user):
    db_session.add(user)
    db_session.commit()


if __name__ == '__main__':
    user = User('dongliu84', password='efe572d63d122f7da7f01d18e076fed8', email='im@dongliu.net', level=LEVEL_ADMIN)
    save_user(user.username, user)