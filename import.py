from datetime import datetime
from db.commentdb import Comment
from db.filedb import File
from db.postdb import Post
from db.tagdb import Tag

__author__ = 'dongliu'

import os
import requests
import json
import base64

from db import postdb, tagdb, tagrdb, filedb, commentdb


os.chdir(os.path.dirname(__file__))

_id = 622413
id_dict = {}


def import_posts():
    resp = requests.get('http://www.dongliu.net/export/posts')
    ids = json.loads(resp.text)['ids']
    resp.close()
    for id in ids:
        import_post(id)
    print id_dict


def import_post(postid):
    print 'http://www.dongliu.net/export/post/%d' % postid
    resp = requests.get('http://www.dongliu.net/export/post/%d' % postid)
    post = json.loads(resp.text)
    resp.close()
    save_post(post)


def save_post(p):
    post = Post(id=p["id"], title=p["title"], date=_time(p["date"]), author=p["author"],
                content=p["content"], privilege=p["privilege"], last_modify_date=_time(p["last_modify_date"]),
                last_modify_by=p["last_modify_by"], commentCount=p["commentCount"])
    global _id
    global id_dict
    if post.id > _id:
        id_dict[post.id] = _id
        post.id = _id
        _id += 1

    if post.commentCount is None:
        post.commentCount = 0
    post.tags = p["tags"]
    # postdb.m_save_post(post)


id_map = {5226880978386944: 622416, 5681726336532480: 622418, 5855096986402816: 622437, 5785905063264256: 622422,
          5336755368624128: 622417, 5846867829063680: 622434, 5821725090512896: 622428, 5839069779066880: 622432,
          5883392230948864: 622442, 634142: 622414, 5823451667365888: 622429, 5906765174538240: 622448,
          5901448273461248: 622446, 5896921377931264: 622445, 5790277339971584: 622424, 5871384039260160: 622440,
          5841138074255360: 622433, 5812155903377408: 622426, 5880084837695488: 622441, 5789830931808256: 622423,
          5733953138851840: 622419, 5893431851220992: 622444, 632142: 622413, 5865619656278016: 622439,
          5819225956417536: 622427, 5906310176440320: 622447, 5856755649085440: 622438, 5850930868125696: 622435,
          5771822402371584: 622420, 5830784250281984: 622431, 5828320147013632: 622430, 636142: 622415,
          5778384575528960: 622421, 5803163852472320: 622425, 5851833348128768: 622436, 5886836794720256: 622443}


def import_comments():
    resp = requests.get('http://www.dongliu.net/export/posts')
    ids = json.loads(resp.text)['ids']
    resp.close()
    for id in ids:
        import_comment(id)


def import_comment(postid):
    resp = requests.get('http://www.dongliu.net/export/comment/%d' % postid)
    comments = json.loads(resp.text)['comments']
    resp.close()
    print "post:%d, comment count:%d" % (postid, len(comments))
    for comment in comments:
        save_comment(comment)


def save_comment(c):
    comment = Comment(postId=c["postId"], date=_time(c["date"]), author=c["author"],
                      content=c["content"], parentContent=c["parentContent"], username=c["username"],
                      homepage=c["homepage"], email=c["email"], ip=c["ip"])
    if comment.postId in id_map:
        comment.postId = id_map[comment.postId]
    if comment.ip is None:
        comment.ip = ''
    commentdb.save_comment(comment)


def import_tags():
    resp = requests.get('http://www.dongliu.net/export/tags')
    tags = json.loads(resp.text)['tags']
    resp.close()
    for tag in tags:
        tagdb.m_save(Tag(cid=tag['cid'], name=tag['name'], count=tag['count']))


def import_files():
    resp = requests.get('http://www.dongliu.net/export/files')
    ids = json.loads(resp.text)['files']
    resp.close()
    for fid in ids:
        import_file(fid)


def import_file(fid):
    fid = str(fid)
    print fid
    resp = requests.get('http://www.dongliu.net/export/file/%s' % fid)
    ff = json.loads(resp.text)
    resp.close()
    b = base64.b64decode(ff['content'])
    sub_dir = 'upload/' + fid[0:1] + '/' + fid[0:2]
    try:
        os.makedirs(sub_dir)
    except:
        pass
    with open(sub_dir + '/' + fid, 'w+') as f:
        f.write(b)
    fil = File(fileid=fid, mimeType=ff['mimeType'], date=_time(ff['date']), fileName=ff['fileName'])
    filedb.save_file(fil)

def _time(time_str):
    if time_str is None:
        return None
    return datetime.strptime(time_str, '%Y-%m-%d %H:%M:%S')


if __name__ == '__main__':
    import_files()