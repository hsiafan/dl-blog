# encoding=utf-8

import os

from service import users


os.chdir(os.path.dirname(__file__))

import urllib
import logging
from logging.handlers import RotatingFileHandler

from flask import Flask

from config import blog_config

from db.commentdb import *
from tools import text_util
from web.admin_view import admin_view
from web.captcha_view import captcha_view
from web.comment_view import comment_view
from web.file_view import file_view
from web.post_view import post_view
from web.search_view import search_view
from web.tools_view import tools_view
from web.user_view import user_view
from db import postdb, tagdb, commentdb

app = Flask(__name__)
app.register_blueprint(admin_view)
app.register_blueprint(captcha_view)
app.register_blueprint(comment_view)
app.register_blueprint(file_view)
app.register_blueprint(post_view)
app.register_blueprint(search_view)
app.register_blueprint(tools_view)
app.register_blueprint(user_view)


# url encode fuc for jinja
def urlencode(text):
    return urllib.quote(text.encode('utf-8'))


@app.template_filter('tag')
def encode_tag(tag):
    return text_util.encode_tag(tag)


def get_spin_posts():
    return postdb.get_post_list(postdb.PRIVILEGE_SPIN, 0, 5)


def get_recent_posts():
    return postdb.get_post_list(postdb.PRIVILEGE_SHOW, 0, blog_config.recent_post_num)


def get_archives():
    archives = tagdb.get_tag_list(tagdb.CID_ARCHIVE)
    archives.sort(key=lambda a: a.name, reverse=True)
    return archives


def get_all_tags():
    tags = tagdb.get_tag_list(tagdb.CID_TAG)
    tags.sort(key=lambda a: a.count, reverse=True)
    return tags


def get_recent_comments():
    comment_list = commentdb.get_comments(0, blog_config.recent_comment_num)
    for comment in comment_list:
        comment.content = text_util.sub_str(comment.content, 33 * 2)
    return comment_list


def get_fortune():
    from service import fortune

    return fortune.rand_fortune().decode('utf-8')


def get_post_title(post_id):
    return postdb.get_post(post_id).title


@app.template_filter('datetime')
def format_datetime(date):
    if date is None:
        return ''
    date = date + timedelta(hours=8)
    return date.strftime('%Y-%m-%d %H:%M')


@app.template_filter('datetimelocal')
def format_datetime_local(date):
    """used with html5 datetime-local input type"""
    if date is None:
        return ''
    date = date + timedelta(hours=8)
    return date.strftime('%Y-%m-%dT%H:%M')


@app.template_filter('before')
def substring_before(string, dem):
    idx = string.find(dem)
    if idx < 0:
        return string
    return string[0:idx]


# for jinja functions
app.jinja_env.globals.update({
    'getuser': users.get_current_user,
    'isadmin': users.is_current_user_admin,

    'get_spin_posts': get_spin_posts,
    'get_recent_posts': get_recent_posts,
    'get_recent_comments': get_recent_comments,
    'get_all_tags': get_all_tags,
    'get_archives': get_archives,
    'get_fortune': get_fortune,
    'get_post_title': get_post_title,
    'encode': urlencode,
})

# set log format and level
try:
    os.mkdir('logs')
except Exception as e:
    pass
handler = RotatingFileHandler('logs/blog.log', maxBytes=10 * 1024 * 1024, backupCount=7)
handler.setLevel(logging.INFO)
app.logger.addHandler(handler)


@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()


app.secret_key = 'LHU*&)^&TFIYO:&)(&*RFOGLKJGFO**(RD&OCV'

if __name__ == '__main__':
    # this is for local debug
    app.run(debug=True, host='127.0.0.1')
