# encoding=utf-8
import os
import hashlib

from flask import (request, jsonify, abort, send_from_directory)
from flask import Blueprint

from config import upload_path
from db.filedb import File
from service import users
from db import filedb


file_view = Blueprint('file_view', __name__)

CACHE_DURATION = 24 * 60 * 60 * 1000


@file_view.route('/showfile/<fileid>', methods=['GET'])
@file_view.route('/showimage/<fileid>', methods=['GET'])
def show_file(fileid):
    f = filedb.getfile(fileid)

    if not f:
        abort(404)

    mime_type = f.mimeType.encode('utf-8')
    sub_dir = fileid[0:1] + '/' + fileid[0:2]

    return send_from_directory(os.path.join(upload_path, sub_dir), fileid, mimetype=mime_type,
                               cache_timeout=365 * 24 * 3600, conditional=True)


@file_view.route('/file/upload', methods=['POST'])
def upload_file():
    if not users.is_current_user_admin():
        abort(403)
    file_info = request.files['file']

    content = file_info.getvalue()
    md5 = hashlib.md5()
    md5.update(content)
    fileid = md5.hexdigest()

    sub_path = fileid[0:1] + '/' + fileid[0:2] + '/' + fileid
    try:
        os.makedirs(os.path.join(upload_path, fileid[0:1] + '/' + fileid[0:2]))
    except:
        pass
    with open(os.path.join(upload_path, sub_path), 'wb') as f:
        f.write(content)

    f = File(fileid=fileid, fileName=file_info.filename, mimeType=file_info.mimetype)
    filedb.save_file(f)
    return jsonify({"filelink": "/showfile/%s" % fileid, "filename": file_info.filename})