# coding=utf-8
__author__ = 'dongliu'

import StringIO
import json

from flask import (request, jsonify, abort)
from flask import Blueprint

from config import blog_config, mc
from db.commentdb import Comment
from service import users
from db import postdb, commentdb


comment_view = Blueprint('comment_view', __name__)


@comment_view.route('/comment/<post_id>/<int:page>', methods=['GET'])
def show_comment_list(post_id, page):
    post = postdb.get_post(post_id)
    count = post.commentCount
    if count is None:
        count = 0
    comment_page_size = blog_config.comment_num_per_page
    comment_list = commentdb.get_post_comments(post_id, (page - 1) * comment_page_size,
                                               comment_page_size)

    is_admin = users.is_current_user_admin()
    result = {
        "count": count,
        "page": page,
        "pagecount": (count - 1) / comment_page_size + 1,
        "clist": [comment.to_dict(is_admin) for comment in comment_list]
    }
    return jsonify(result)


def _escape_html(string):
    if not string:
        return string
    buf = StringIO.StringIO()
    for ch in string:
        if ch != '<' and ch != '>' and ch != '&' and ch != '"' and ch != "'":
            buf.write(ch)
    return buf.getvalue()


@comment_view.route('/comment/add', methods=['POST'])
def add_comment():
    post_id = int(request.values.get('postid'))
    user = users.get_current_user_name()
    if not user:
        # check captcha
        captcha = request.values.get('code')
        seq = request.values.get('seq')
        code = mc.get("captcha_" + seq.encode('utf-8'))
        mc.delete("captcha_" + seq.encode('utf-8'))
        if captcha is None or captcha.strip().upper() != code:
            return jsonify({'state': 1, 'msg': u'验证码错误'})

    post = postdb.get_post(post_id)
    if post is None:
        return jsonify({'state': -1, 'msg': u'不存在的文章'})

    comment = Comment(postId=post_id, content=request.values.get('content'),
                      username=request.values.get('username'),
                      email=_escape_html(request.values.get('email')),
                      homepage=_escape_html(request.values.get('homepage')),
                      ip=request.remote_addr
    )

    reply_to = request.values.get('replyto')
    if reply_to:
        parent_comment = commentdb.getcomment(int(reply_to))
        parent_comment = parent_comment.to_pdict()
        if parent_comment:
            comment.parentContent = json.dumps(parent_comment)
    if not comment.email:
        comment.email = "default@default.com"
    if comment.homepage and not comment.homepage.startswith('http'):
        comment.homepage += 'http://'

    comment_id = commentdb.save_comment(comment)

    if post.commentCount is None:
        post.commentCount = 0
    post.commentCount += 1
    postdb.save_post(post)

    return jsonify({'state': 0, 'msg': ''})


@comment_view.route('/comment/delete', methods=['POST'])
def delete_comment():
    if not users.is_current_user_admin():
        abort(403)
    comment_id = int(request.values.get('commentid'))
    comment = commentdb.getcomment(comment_id)
    post_id = comment.postId
    commentdb.delete_comment(comment_id)
    post = postdb.get_post(post_id)
    if post.commentCount is None:
        post.commentCount = 0
    post.commentCount -= 1
    if post.commentCount < 0:
        post.commentCount = 0
    postdb.save_post(post)
    return jsonify({'state': 0})