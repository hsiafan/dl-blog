# coding=utf8
from os import abort
from config import blog_config
from service import users
from db import postdb, filedb
from tools.pagertool import *
from flask import (render_template, request)
from flask import Blueprint

admin_view = Blueprint('admin_view', __name__)


@admin_view.route('/admin/file', methods=['GET'])
def list_file():
    if not users.is_current_user_admin():
        abort(403)
    try:
        page = int(request.args.get('page'))
    except:
        page = 1
    total = filedb.count()
    pagesize = 20
    pager = Pager(total, page, pagesize)
    pager.setbase('/admin/file?page=')
    file_list = filedb.get_files((page - 1) * pagesize, pagesize)
    return render_template('admin/file.html', filelist=file_list, pager=pager, config=blog_config)


@admin_view.route('/admin/post', methods=['GET'])
def get():
    """admin hidden post"""
    if not users.is_current_user_admin():
        return
    try:
        page = int(request.args.get('page'))
    except:
        page = 1
    total = postdb.count(postdb.PRIVILEGE_HIDE)
    pagesize = 20
    pager = Pager(total, page, pagesize)
    pager.setbase('/admin/post?page=')
    post_list = postdb.get_post_list(postdb.PRIVILEGE_HIDE, (page - 1) * pagesize, pagesize)
    return render_template('admin/post.html', postlist=post_list, pager=pager, config=blog_config)