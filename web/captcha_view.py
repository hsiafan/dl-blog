# encoding=utf-8
__author__ = 'dongliu'

import StringIO
from flask import (send_file, request, jsonify)
from flask import Blueprint

from config import mc
from service import captcha

captcha_view = Blueprint('captcha_view', __name__)


@captcha_view.route('/captcha', methods=['GET'])
def show_captcha():
    seq = request.args.get('seq')
    mstream = StringIO.StringIO()
    code_img, code = captcha.create_validate_code()
    mc.set("captcha_" + seq.encode('utf-8'), code, time=1800)
    code_img.save(mstream, "GIF")
    mstream.seek(0)
    return send_file(mstream, mimetype='image/gif')


@captcha_view.route('/captcha/check', methods=['POST'])
def check_captcha():
    seq = request.values.get('seq')
    code = request.values.get('code')
    if not seq or not code:
        return jsonify({"state": -1})
    ocode = mc.get("captcha_" + seq.encode('utf-8'))
    if not ocode or code.strip().upper() != ocode:
        return jsonify({"state": 1})
    return jsonify({"state": 0})