# coding=utf-8
import hashlib

from flask import (redirect, request, render_template, session)
from flask import Blueprint

from db import userdb

user_view = Blueprint('user_view', __name__)


@user_view.route('/login', methods=['GET'])
def login():
    return render_template("login.html")


@user_view.route('/login', methods=['POST'])
def do_login():
    username = request.form['username']
    password = request.form['password']
    if not username or not password:
        return render_template("login.html", msg=u"用户名或密码为空")
    user = userdb.get_user(username)
    if user is None:
        return render_template("login.html", msg=u"用户名不存在")
    md5 = hashlib.md5()
    md5.update(username + password)
    password = md5.hexdigest()
    if password != user.password:
        return render_template("login.html", msg=u"密码不正确")

    session['username'] = username
    referrer = request.headers.get('Referrer')
    if not referrer:
        referrer = ""
    return redirect(referrer)


@user_view.route('/logout', methods=['GET'])
def logout():
    referrer = request.headers.get('Referrer')
    if not referrer:
        referrer = ""
    del session['username']
    return redirect(referrer)