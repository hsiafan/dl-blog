# coding=utf-8
from service import users

from service.fortune import *
from flask import (Response, request, abort)
from pygments import highlight
from pygments.formatters.html import HtmlFormatter
from pygments.lexers import get_lexer_by_name
from flask import Blueprint

tools_view = Blueprint('tools_view', __name__)


@tools_view.route('/tools/fortune', methods=['GET', 'POST'])
def fortune():
    return Response(rand_fortune(), mimetype='text/plain')


@tools_view.route('/tools/highlight', methods=['GET', 'POST'])
def highlight_code():
    if not users.is_current_user_admin():
        return
    language = request.values.get('language', None)
    code = request.values.get('code', None)
    if not language or not code:
        abort(400)
    lexer = get_lexer_by_name(language, stripall=True)
    return Response(highlight(code, lexer, HtmlFormatter(nowrap=False)), mimetype='text/html')
