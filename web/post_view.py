# coding=utf-8
from datetime import datetime, timedelta

import PyRSS2Gen
from flask import (Response, render_template, abort, request, redirect, jsonify)
from flask import Blueprint

from config import blog_config
from db.postdb import Post
from service import users, pinyin
from tools import text_util
from db import postdb, tagdb, commentdb
from tools.pagertool import *
from tools import platform

post_view = Blueprint('post_view', __name__)


def _empty_post():
    post = Post(title='', content='', date=datetime.today(), privilege=1, commentCount=0,
                last_modify_by=datetime.today())
    return post


def _post_public(post):
    return post is not None and post.privilege == postdb.PRIVILEGE_SHOW


@post_view.route('/', methods=['GET'], defaults={'tag': None, "archive": None, "page_num": 1})
@post_view.route('/post', methods=['GET'], defaults={'tag': None, "archive": None, "page_num": 1})
@post_view.route('/post/list/<int:page_num>', methods=['GET'],
                 defaults={'tag': None, "archive": None})
@post_view.route('/post/list/<tag>', methods=['GET'], defaults={"page_num": 1, "archive": None})
@post_view.route('/post/list/<tag>/', methods=['GET'], defaults={"page_num": 1, "archive": None})
@post_view.route('/post/list/<tag>/<int:page_num>', methods=['GET'], defaults={"archive": None})
@post_view.route('/post/archive/<archive>', methods=['GET'], defaults={"page_num": 1, "tag": None})
@post_view.route('/post/archive/<archive>/', methods=['GET'], defaults={"page_num": 1, "tag": None})
@post_view.route('/post/archive/<archive>/<int:page_num>', methods=['GET'], defaults={"tag": None})
def show_post_list(page_num, tag, archive):
    """show post list"""
    config = blog_config

    if tag:
        tag = tag.replace('%2F', '/')
        total_tag = tagdb.get_tag(tagdb.CID_TAG, tag)
        if total_tag is None:
            abort(404)
        total = total_tag.count
    elif archive:
        total_tag = tagdb.get_tag(tagdb.CID_ARCHIVE, archive)
        if total_tag is None:
            abort(404)
        total = total_tag.count
    else:
        total = postdb.count(postdb.PRIVILEGE_SHOW)

    pager = Pager(total, page_num, config.post_num_per_page)

    if tag:
        base = '/post/list/' + text_util.encode_tag(tag) + '/'
    elif archive:
        base = '/post/archive/' + archive + '/'
    else:
        base = '/post/list/'
    pager.setbase(base)

    post_list = postdb.get_post_list(privilege=postdb.PRIVILEGE_SHOW,
                                     offset=pager.offset,
                                     limit=pager.pagesize,
                                     tag=tag,
                                     archive=archive)

    pf = platform.get_platform(request.headers.get('User-Agent'))
    if pf == platform.PHONE:
        tpl = "m/m_post_list.html"
    else:
        tpl = "post_list.html"
    return render_template(tpl, postlist=post_list, pager=pager, tag=tag, archive=archive,
                           config=blog_config)


@post_view.route('/post/<int:post_id>', methods=['GET'])
def view_post(post_id):
    """ show detail post."""
    post = postdb.get_post(post_id)
    if post is None:
        return abort(404)
    if post.privilege <= 0 and not users.is_current_user_admin():
        return abort(404)

    # get similar posts
    if post.tags:
        similar_posts = postdb.get_post_list(privilege=postdb.PRIVILEGE_SHOW,
                                             offset=0,
                                             limit=10,
                                             tag=post.tags[0])
        similar_posts = [s for s in similar_posts if s.id != post_id]
        if len(similar_posts) > 4:
            start = post_id % (len(similar_posts) - 4)
            similar_posts = similar_posts[start:start + 4]
    else:
        similar_posts = []

    pf = platform.get_platform(request.headers.get('User-Agent'))
    if pf == platform.PHONE:
        tpl = "m/m_post_view.html"
    else:
        tpl = "post_view.html"
    return render_template(tpl, post=post, similars=similar_posts, config=blog_config)


@post_view.route('/post/edit', methods=['GET'])
def edit_post():
    """show post new/edit page."""
    if not users.is_current_user_admin():
        return abort(403)

    post_id_str = request.args.get('postid')
    if post_id_str:
        # edit
        post_id = int(post_id_str)
        post = postdb.get_post(post_id)
    else:
        # new
        post = _empty_post()
    all_tags = tagdb.get_tag_list(tagdb.CID_TAG)
    tag_list = [{"name": tag.name, "spell": pinyin.get_pinyin(tag.name)} for tag in all_tags if tag]
    return render_template('post_edit.html', post=post, taglist=tag_list, config=blog_config)


@post_view.route('/post/update', methods=['POST'])
def update_post():
    """ add or update post """
    if not users.is_current_user_admin():
        return abort(403)
    post_id_str = request.values.get('postid')
    if not post_id_str:
        post_id = None
    else:
        post_id = int(post_id_str)

    if post_id is not None:
        post = postdb.get_post(post_id)
        old_post = Post()
        old_post.privilege = post.privilege
        old_post.tags = post.tags
        old_post.date = post.date
        post.last_modify_date = datetime.today()
        post.last_modify_by = users.get_current_user_name()
    else:
        post = _empty_post()
        old_post = None
        post.author = users.get_current_user_name()
    post.title = request.values.get('title')
    post.content = request.values.get('content')
    post.tags = [tag for tag in request.values.getlist('tags') if tag]
    post.privilege = int(request.values.get('privilege'))
    post.date = datetime.strptime(request.values.get('pubdate'), '%Y-%m-%dT%H:%M') - timedelta(
        hours=8)
    new_post_id = postdb.save_post(post)
    _post_change(old_post, post)
    return redirect('/post/' + str(new_post_id))


@post_view.route('/post/delete', methods=['POST'])
def delete_post():
    """delete a post.now it is implemented by mark the status to be PRIVILEGE_DEL."""

    if not users.is_current_user_admin():
        return abort(403)
    post_id_str = request.values.get('postid')
    if not post_id_str:
        return abort(404)
    post = postdb.get_post(int(post_id_str))
    if post:
        _post_change(post, None)
        post.privilege = postdb.PRIVILEGE_DEL
        post.commentCount = 0
        postdb.save_post(post)
        commentdb.delete_post_comment(post.id)
        return jsonify({'state': 0, 'msg': ''})

    return jsonify({'state': 1, 'msg': u'此文章不存在'})


def _post_change(old_post, post):
    """
    deal with tags, counts, and index when post have been changed.
    """
    tags = []
    if old_post and old_post.tags:
        tags.extend(old_post.tags)
    if post and post.tags:
        tags.extend(post.tags)
    for tag in tags:
        count = postdb.count_tag(postdb.PRIVILEGE_SHOW, tag)
        tagdb.set_tag_count(tagdb.CID_TAG, tag, count)

    if old_post:
        archive = text_util.get_archive(old_post.date)
        count = postdb.count_archive(postdb.PRIVILEGE_SHOW, archive)
        tagdb.set_tag_count(tagdb.CID_ARCHIVE, archive, count)
    if post:
        archive = text_util.get_archive(post.date)
        count = postdb.count_archive(postdb.PRIVILEGE_SHOW, archive)
        tagdb.set_tag_count(tagdb.CID_ARCHIVE, archive, count)


@post_view.route('/feed', methods=['GET'], defaults={'tag': None})
@post_view.route('/feed/<tag>', methods=['GET'])
def feed(tag):
    """rss 2.0 feed"""
    if tag:
        tag = tag.replace('%2F', '/')
    config = blog_config
    post_list = postdb.get_post_list(privilege=postdb.PRIVILEGE_SHOW,
                                     offset=0,
                                     limit=config.feed_num,
                                     tag=tag)
    title = config.heading
    if tag:
        title += u" - " + tag
    link = "http://" + config.host
    description = config.subheading
    items = []
    for post in post_list:
        items.append(PyRSS2Gen.RSSItem(
            title=post.title,
            link="http://" + config.host + "/post/" + str(post.id),
            description=post.content,
            pubDate=post.date,
        ))
    rss = PyRSS2Gen.RSS2(
        title=title,
        link=link,
        description=description,
        items=items,
    )
    return Response(rss.to_xml(encoding="utf-8"), mimetype='application/rss+xml')