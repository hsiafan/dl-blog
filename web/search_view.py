# coding=utf-8

__author__ = 'dongliu'

from flask import (render_template, request)
from flask import Blueprint

from config import blog_config


search_view = Blueprint('search_view', __name__)


@search_view.route('/search', methods=['GET'])
def search_page():
    return render_template('search.html', query=request.args.get('query'), config=blog_config)


@search_view.route('/search/ajax', methods=['GET'])
def search_api():
    # query = request.args.get('query')
    # cursor = request.args.get('cursor')
    # pagesize = int(request.args.get('pagesize'))
    # result = postindex.query(query, cursor, pagesize)
    # return jsonify(result)
    return None