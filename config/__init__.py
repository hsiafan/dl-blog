# coding=utf-8
import memcache

memcached_address = '127.0.0.1:11211'
mc = memcache.Client([memcached_address], debug=0)

class BlogConfig:
    def __init__(self):
        self.heading = u"嘴角上扬微笑"
        self.subheading = u""
        self.host = u'dongliu.net'
        self.abstract = False
        self.post_num_per_page = 10
        self.comment_num_per_page = 10
        self.feed_num = 10
        self.recent_comment_num = 10
        self.recent_post_num = 10


blog_config = BlogConfig()
upload_path = "upload"

date_base_url = 'mysql+mysqlconnector://dl_blog:dl_blog@127.0.0.1:3306/dl_blog'