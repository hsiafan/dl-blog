from flask import session
from db import userdb


def is_current_user_admin():
    user = get_current_user()
    if not user:
        return False
    return user.level == userdb.LEVEL_ADMIN


def get_current_user():
    username = session.get('username')
    if not username:
        return None
    return userdb.get_user(username)


def get_current_user_name():
    return session.get('username')