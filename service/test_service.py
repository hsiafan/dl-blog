import unittest
from service.pinyin import get_pinyin


class TestPinyin(unittest.TestCase):
    def setUp(self):
        pass

    def test_pinyin(self):
        py = get_pinyin(u'中华')
        assert u'zhonghua' == py
