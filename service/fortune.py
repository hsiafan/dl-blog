import random


def load_fortune(datafile="res/fortune.txt"):
    fortune_list = []
    with open(datafile) as fortune_file:
        for line in fortune_file:
            line = line.strip()
            fortune_list.append(line)
    return fortune_list


_fortune_list = load_fortune()


def rand_fortune():
    idx = random.randint(0, len(_fortune_list) - 1)
    return _fortune_list[idx]

