from pypinyin import lazy_pinyin


def get_pinyin(hanzi):
    """
    :type hanzi: unicode
    """
    if not isinstance(hanzi, unicode):
        raise Exception("Must be unicode type.")
    if hanzi is None or len(hanzi) == 0:
        return u''
    return u''.join(lazy_pinyin(hanzi))

